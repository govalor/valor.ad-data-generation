﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class TeacherNWPOST
    {
        public class ZnwWorkflowInstance
        {
        }

        public class AppData
        {
            public object nwId { get; set; }
            public int JVEA7RecordsID { get; set; }
            public string JVFirstName { get; set; }
            public string JVLastName { get; set; }
            public string JVSchoolIDForFacutly { get; set; }
            public int JVFacultyID { get; set; }
            public string EmailAddress { get; set; }
            public string JVDepartment { get; set; }
            public object nwExternalId { get; set; }
            public object nwCreatedByUser { get; set; }
            public object nwCreatedDate { get; set; }
            public object nwLastModifiedByUser { get; set; }
            public string nwLastModifiedDate { get; set; }
            public object nwTenantStripe { get; set; }
            public object AttachmentGroupId { get; set; }
            public ZnwWorkflowInstance znwWorkflowInstance { get; set; }
            public object znwOwner { get; set; }
            public bool znwLocked { get; set; }
            public object znwSparseOwner { get; set; }
            public object nwNateId { get; set; }
            public object nwNateDisposition { get; set; }
            public object InternalSeq { get; set; }
            public string nwBaseVersion { get; set; }
        }

        public class Record
        {
            public AppData appData { get; set; }
            public string version { get; set; }
            public string nateDisposition { get; set; }
        }

        public class ClientState
        {
            public string RootNwId { get; set; }
            public string SaveType { get; set; }
            public string ApplicationName { get; set; }
        }

        public class RootObject
        {
            public List<Record> records { get; set; }
            public ClientState clientState { get; set; }
            public bool createContainer { get; set; }
            public bool commitContainer { get; set; }
        }
    }
}
