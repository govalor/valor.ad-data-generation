﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MegaModel
{
    public class ADUserModel
    {
        public int EA7RecordsID { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OULocation { get; set; }
        public string OUDefaultGroup { get; set; }
        public string OUClassGroup { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
