﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MegaModel;
using Newtonsoft;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Authenticators;

namespace DataGeneration
{
    class Program
    {
        static void Main(string[] args)
        {
            GenerateRecord generator = new GenerateRecord();
            ModelTranslation translator = new ModelTranslation();
            StudentModel.RootObject studentRoot = new StudentModel.RootObject();

            Model.RootObject root = generator.getModel(20);
            List<StudentModel.Record> studentList = translator.toADStudentList(root);

            //add the list to a root to be posted
            studentRoot.records = studentList;

            //testing
            //translator.printStudentADList(studentList);
            //Console.ReadLine();

            //post request
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMActiveDirectory");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(studentRoot), ParameterType.RequestBody); // adds to POST or URL querystring based on Method

            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
            Console.ReadLine();
        }

        //for faculty
        /*static void Main(string[] args)
        {
            GenerateRecord generator = new GenerateRecord();
            ModelTranslation translator = new ModelTranslation();
            FacultyStaffModel.RootObject facultyRoot = new FacultyStaffModel.RootObject();

            Model.RootObject root = generator.getModel(21);
            List<FacultyStaffModel.Record> facultyList = translator.toADFacultyStaff(root);

            //add the list to a root to be posted
            facultyRoot.records = facultyList;

            //testing
            //translator.printStudentADList(studentList);
            //Console.ReadLine();

            //post request
            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyAD");
            client.Authenticator = new HttpBasicAuthenticator("cheyanne.miller@nextworld.net", "Pion33rs!");

            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(facultyRoot), ParameterType.RequestBody); // adds to POST or URL querystring based on Method

            // easy async support
            client.ExecuteAsync(request, response =>
            {
                Console.WriteLine(response.Content);
            });
            Console.ReadLine();
        }*/
    }
}
